import React from 'react';

import postReducer from './postReducer';

const PostContext = React.createContext();

function PostProvider({ children }) {
	const initState = { posts: [] };

	const [state, dispatch] = React.useReducer(postReducer, initState);
	const localStorageState = localStorage.getItem('post');

	React.useEffect(() => {
		if (localStorageState) {
			dispatch({
				type: 'SET_POST',
				payload: JSON.parse(localStorageState).posts,
			});
		} else {
			fetch('https://jsonplaceholder.typicode.com/posts')
				.then(response => response.json())
				.then(data => {
					dispatch({ type: 'SET_POST', payload: data });
				});
		}
	}, []);

	React.useEffect(() => {
		localStorage.setItem('post', JSON.stringify(state));
	}, [state]);

	const deletePost = React.useCallback(
		id => dispatch({ type: 'DELETE', payload: id }),
		[]
	);

	const updatePost = React.useCallback(
		newDataPost => dispatch({ type: 'UPDATE', payload: newDataPost }),
		[]
	);

	const createPost = React.useCallback(
		newPost => dispatch({ type: 'CREATE', payload: newPost }),
		[]
	);

	return (
		<PostContext.Provider
			value={{ ...state, deletePost, updatePost, createPost }}>
			{children}
		</PostContext.Provider>
	);
}

export { PostContext, PostProvider };
