function postReducer(state, action) {
	switch (action.type) {
		case 'SET_POST': {
			console.log('SET_POST');
			const posts = action.payload;
			return { ...state, posts: posts };
		}

		case 'DELETE': {
			console.log('DELETE');
			const id = action.payload;

			return {
				...state,
				posts: state.posts.filter(post => String(post.id) !== id),
			};
		}

		case 'UPDATE': {
			console.log('UPDATE');
			const newDataPost = action.payload;

			return {
				...state,
				posts: state.posts.map(post =>
					post.id === newDataPost.id ? { ...newDataPost } : post
				),
			};
		}

		case 'CREATE': {
			console.log('CREATE');
			const newPost = action.payload;

			return {
				...state,
				posts: [newPost, ...state.posts],
			};
		}

		default: {
			console.log('default');
			return { ...state };
		}
	}
}

export default postReducer;
