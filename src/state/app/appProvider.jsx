import React from 'react';

import { users } from '../../dev/data/devUserData';
import appReducer from './appReducer';

const AppContext = React.createContext();

const startState = {
	currentUser: null,
	users: [...users],
};

function AppProvider({ children }) {
	const localStorageState = localStorage.getItem('app');
	const initState = localStorageState
		? JSON.parse(localStorageState)
		: startState;

	const [state, dispatch] = React.useReducer(appReducer, initState);

	React.useEffect(() => {
		localStorage.setItem('app', JSON.stringify(state));
	}, [state]);

	const sigIn = React.useCallback(
		id => dispatch({ type: 'SING_IN', payload: id }),
		[]
	);

	const sigOut = React.useCallback(() => dispatch({ type: 'SING_OUT' }), []);
	const addUser = React.useCallback(
		data => dispatch({ type: 'ADD_USER', payload: data }),
		[]
	);

	const changeUserData = React.useCallback(
		newUserData => dispatch({ type: 'CHANGE_USER', payload: newUserData }),
		[]
	);

	const getUser = React.useCallback(
		(id, handler) => dispatch({ type: 'GET_USER', payload: { id, handler } }),
		[]
	);

	const removeUser = React.useCallback(
		id => dispatch({ type: 'REMOVE_USER', payload: id }),
		[]
	);

	const context = {
		...state,
		sigIn,
		sigOut,
		addUser,
		changeUserData,
		getUser,
		removeUser,
	};

	return <AppContext.Provider value={context}>{children}</AppContext.Provider>;
}
export { AppProvider, AppContext };
