import { getUser, createUser } from '../../utils/user';
function appReducer(state, action) {
	const findUser = id => getUser(state.users, id);

	switch (action.type) {
		case 'SING_IN': {
			const id = action.payload;
			return {
				...state,
				currentUser: findUser(id) || null,
			};
		}

		case 'SING_OUT': {
			return { ...state, currentUser: null };
		}

		case 'GET_USER': {
			const id = action.payload.id;
			const handler = action.payload.handler;
			const user = findUser(id);
			handler(user);
			return { ...state };
		}

		case 'ADD_USER': {
			const data = action.payload;
			const user = createUser(data);
			return { ...state, users: [user, ...state.users] };
		}

		case 'REMOVE_USER': {
			const id = action.payload;
			return {
				...state,
				users: [...state.users.filter(user => user.id !== id)],
			};
		}

		case 'CHANGE_USER': {
			const newUserData = action.payload;
			return {
				...state,
				users: state.users.map(user =>
					user.id === newUserData.id ? newUserData : user
				),
			};
		}

		default: {
			return { ...state };
		}
	}
}

export default appReducer;
