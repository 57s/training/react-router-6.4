import React from 'react';
import { AppContext } from '../state/app/appProvider';

function useAuth() {
	const { currentUser, sigOut } = React.useContext(AppContext);

	return {
		isUser: currentUser ? true : false,
		isModerator:
			currentUser?.type === 'moderator' || currentUser?.type === 'admin',
		isAdmin: currentUser?.type === 'admin',
		userName: currentUser?.name,
		accountType: currentUser?.type,
		sigOut,
	};
}

export default useAuth;
