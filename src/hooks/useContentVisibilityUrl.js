import { useLocation } from 'react-router-dom';

function useContentVisibilityUrl(arrUrl) {
	const location = useLocation();
	const currentUrl = location.pathname;
	const isShowContent = arrUrl.find(url => url === currentUrl);
	return { isShow: !isShowContent };
}

export default useContentVisibilityUrl;
