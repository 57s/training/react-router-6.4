import { useNavigate } from 'react-router-dom';

function useCustomNavigate() {
	const navigate = useNavigate();

	const goBack = () => navigate(-1);
	const goHome = () => navigate('/');

	return { goBack, goHome };
}

export default useCustomNavigate;
