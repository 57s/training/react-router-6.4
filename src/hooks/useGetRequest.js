import React from 'react';

const useGetRequest = function (url) {
	const [data, setData] = React.useState(false);

	React.useEffect(() => {
		fetch(url)
			.then(answer => answer.json())
			.then(data => setData(data));
	}, [url]);

	return data;
};

export { useGetRequest };
