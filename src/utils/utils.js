function changeObjectProperty(object, property, value) {
	return {
		...object,
		[property]: value,
	};
}

export { changeObjectProperty };
