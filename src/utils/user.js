export const getUser = function (state, id) {
	return state.find(user => user.id === id);
};

export const createUser = function (data) {
	const user = {
		id: window.crypto.randomUUID(),
		...data,
		timeCreation: new Date(),
	};
	return user;
};
