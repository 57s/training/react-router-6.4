import React from 'react';

import Container from '../../../components/templates/container/Container';
import Button from '../../../components/atoms/button/Button';
import DevToolsPanelContent from '../devToolsPanelContent/DevToolsPanelContent';
import styles from './DevToolsPanelLayout.module.scss';

function DevToolsPanelLayout(onToggle) {
	return (
		<div className={styles.devToolsPanel}>
			<header className={styles.devToolsPanel_header}>
				<Container>
					<div className={styles.devToolsPanel_headerInner}>
						<h2 className={styles.devToolsPanel_title}>DevToolsPanel</h2>
						<Button onClick={onToggle} name="Закрыть" />
					</div>
				</Container>
			</header>

			<div className={styles.devToolsPanel_main}>
				<Container>
					<div className={styles.devToolsPanel_mainInner}>
						<DevToolsPanelContent onClose={onToggle} />
					</div>
				</Container>
			</div>

			<footer className={styles.devToolsPanel_footer}>
				<Container>
					<div className={styles.devToolsPanel_footerInner}></div>
				</Container>
			</footer>
		</div>
	);
}
export default DevToolsPanelLayout;
