import React from 'react';

import useDevTools from './useDevTools';
import Button from '../../../components/atoms/button/Button';
import styles from './DevToolsPanelContent.module.scss';

function DevToolsPanelContent(props) {
	const { loginAdmin, loginV3, loginTest, loginModerator } = useDevTools(props);
	return (
		<div className={styles.devContent}>
			<div className={styles.devContent_buttonsBox}>
				<Button name="Login: Admin" onClick={loginAdmin} />
				<Button name="Login: Moderator" onClick={loginModerator} />
				<Button name="Login: V3" onClick={loginV3} />
				<Button name="Login: Test" onClick={loginTest} />
			</div>
		</div>
	);
}
export default DevToolsPanelContent;
