import React from 'react';

import {
	devUserAdmin,
	devUserV3,
	devUserTest,
	devUserModerator,
} from '../../data/devUserData';
import { AppContext } from '../../../state/app/appProvider';

function useDevTools({ onClose }) {
	const { sigIn } = React.useContext(AppContext);

	const loginAdmin = () => {
		sigIn(devUserAdmin.id);
		onClose();
	};

	const loginV3 = () => {
		sigIn(devUserV3.id);
		onClose();
	};

	const loginTest = () => {
		sigIn(devUserTest.id);
		onClose();
	};

	const loginModerator = () => {
		sigIn(devUserModerator.id);
		onClose();
	};

	return { loginAdmin, loginV3, loginTest, loginModerator };
}

export default useDevTools;
