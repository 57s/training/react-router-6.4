import React from 'react';
import { createPortal } from 'react-dom';

import DevToolsPanelLayout from '../devToolsPanelLayout/DevToolsPanelLayout';

const portalDev = document.getElementById('dev-portal');

function DevToolsPanel({ isShow = false, onToggle }) {
	if (isShow) {
		return createPortal(DevToolsPanelLayout(onToggle), portalDev);
	} else {
		return null;
	}
}

export default DevToolsPanel;
