import React from 'react';

import { ReactComponent as DevToolsSvg } from '../../img/dev_tools.svg';
import styles from './DevToolsButton.module.scss';

function DevToolsButton({ onClick }) {
	return (
		<button className={styles.devToolsButton} onClick={onClick}>
			<DevToolsSvg />
		</button>
	);
}
export default DevToolsButton;
