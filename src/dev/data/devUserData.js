export const devUserAdmin = {
	id: '0',
	name: 'admin',
	login: 'admin@gmail.com',
	password: 'admin',
	type: 'admin',
	timeCreation: new Date(0).toISOString(),
};

export const devUserTest = {
	id: '1',
	name: 'test',
	login: 'test@mail.ru',
	password: 'test',
	type: 'user',
	timeCreation: new Date('August 8, 1998').toISOString(),
};

export const devUserModerator = {
	id: '2',
	name: 'kesha',
	login: 'kesha@outlook.com',
	password: 'kesha',
	type: 'moderator',
	timeCreation: new Date('November 16, 1996').toISOString(),
};

export const devUserV3 = {
	id: '3',
	name: 'vvv',
	login: 'v3@yandex.ru',
	password: 'vvv',
	type: 'user',
	timeCreation: new Date('February 27, 1998').toISOString(),
};

export const users = [devUserAdmin, devUserTest, devUserV3, devUserModerator];
