import { defer, json } from 'react-router-dom';

import { getAllTodos, getTodo } from '../services/todos';

// esLint ignore
const todosAllLoader = async () => {
	const post = await getAllTodos();

	if (!post.length) {
		throw json(
			{
				message: 'Error Fetch',
				reason: 'Wrong URL',
			},
			{ status: 404, statusText: 'Not found' }
		);
	}
	return defer({
		todos: getAllTodos(),
	});
};

const todoLoader = async ({ params }) => {
	const id = params.todoId;
	const todo = await getTodo(id);

	return defer(todo);
};

export { todosAllLoader, todoLoader };
