import {
	Route,
	Navigate,
	createBrowserRouter,
	createRoutesFromElements,
} from 'react-router-dom';

import Layout from '../components/templates/layout/Layout';
import PageAdminPanel from '../components/pages/adminPanel/PageAdminPanel';
import PageUser from '../components/pages/user/PageUser';
import PageUserEdit from '../components/pages/userEdit/PageUserEdit';
import PageUserCreate from '../components/pages/userCreate/PageUserCreate';
import PageAccount from '../components/pages/account/PageAccount';
import PageAccountEdit from '../components/pages/accountEdit/PageAccountEdit';
import PageRegistration from '../components/pages/registration/PageRegistration';
import PageLogin from '../components/pages/login/PageLogin';
import PageHome from '../components/pages/home/PageHome';
import PagePosts from '../components/pages/posts/PagePosts';
import PagePostCreate from '../components/pages/postCreate/PagePostCreate';
import PagePostEdit from '../components/pages/postEdit/PagePostEdit';
import PagePostSingle from '../components/pages/postSingle/PagePostSingle';
import PageBlog from '../components/pages/blog/PageBlog';
import PageAbout from '../components/pages/about/PageAbout';
import PageAchievements from '../components/pages/achievements/PageAchievements';
import PageContacts from '../components/pages/contacts/PageContacts';
import PageClients from '../components/pages/clients/PageClients';
import PageHistory from '../components/pages/history/PageHistory';
import PagePartners from '../components/pages/partners/PagePartners';
import PageReviews from '../components/pages/reviews/PageReviews';
import PageTeam from '../components/pages/team/PageTeam';
import PageWhoAreWe from '../components/pages/whoAreWe/PageWhoAreWe';
import PageNotFound from '../components/pages/notFound/PageNotFound';
import RequireAuth from '../components/templates/requireAuth/RequireAuth';
import RequireAdmin from '../components/templates/requireAdmin/RequireAdmin';
import RequireModerator from '../components/templates/requireModerator/RequireModerator';

import { PostProvider } from '../state/posts/postProvider';

const routers = createBrowserRouter(
	createRoutesFromElements(
		<Route path="/" element={<Layout />}>
			<Route index element={<PageHome />} />
			<Route path="registration" element={<PageRegistration />} />
			<Route path="login" element={<PageLogin />} />
			<Route path="reg" element={<Navigate to="/registration" replace />} />
			<Route path="log" element={<Navigate to="/login" replace />} />

			<Route
				path="admin-panel"
				element={
					<RequireAdmin>
						<PageAdminPanel />
					</RequireAdmin>
				}
			/>

			<Route
				path="admin-panel/user/"
				element={
					<RequireAdmin>
						<PageUser />
					</RequireAdmin>
				}
			/>

			<Route
				path="admin-panel/user/create"
				element={
					<RequireAdmin>
						<PageUserCreate />
					</RequireAdmin>
				}
			/>

			<Route
				path="admin-panel/user/edit/:userId"
				element={
					<RequireAdmin>
						<PageUserEdit />
					</RequireAdmin>
				}
			/>

			<Route
				path="admin-panel/user/edit"
				element={<Navigate to="/admin-panel/user" replace />}
			/>

			<Route
				path="account"
				element={
					<RequireAuth>
						<PageAccount />
					</RequireAuth>
				}
			/>

			<Route
				path="posts"
				element={
					<PostProvider>
						<PagePosts />
					</PostProvider>
				}
			/>

			<Route
				path="posts/:postId"
				element={
					<PostProvider>
						<PagePostSingle />
					</PostProvider>
				}
			/>

			<Route
				path="account/edit"
				element={
					<RequireAuth>
						<PageAccountEdit />
					</RequireAuth>
				}
			/>

			<Route
				path="posts/new"
				element={
					<RequireAuth>
						<PostProvider>
							<PagePostCreate />
						</PostProvider>
					</RequireAuth>
				}
			/>

			<Route
				path="posts/:postId/edit"
				element={
					<RequireModerator>
						<PostProvider>
							<PagePostEdit />
						</PostProvider>
					</RequireModerator>
				}
			/>

			<Route path="about" element={<PageAbout />}>
				<Route index element={<PageWhoAreWe />} />
				<Route path="achievements" element={<PageAchievements />} />
				<Route path="contacts" element={<PageContacts />} />
				<Route path="history" element={<PageHistory />} />
				<Route path="partners" element={<PagePartners />} />
				<Route path="reviews" element={<PageReviews />} />
				<Route path="team" element={<PageTeam />} />
				<Route path="clients" element={<PageClients />} />
			</Route>

			<Route path="blog" element={<PageBlog />} />
			<Route path="*" element={<PageNotFound />} />
		</Route>
	)
);

export default routers;
