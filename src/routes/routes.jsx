import { Navigate, createBrowserRouter } from 'react-router-dom';

import { todosAllLoader } from '../loaders/todos';
import { todoLoader } from '../loaders/todos';

import { actionCreateTodo } from '../actions/todos';
import { actionUpdateTodo } from '../actions/todos';

import Layout from '../components/templates/layout/Layout';
import PageError from '../components/pages/error/PageError';
import PageAdminPanel from '../components/pages/adminPanel/PageAdminPanel';
import PageUser from '../components/pages/user/PageUser';
import PageUserEdit from '../components/pages/userEdit/PageUserEdit';
import PageUserCreate from '../components/pages/userCreate/PageUserCreate';
import PageModeratorPanel from '../components/pages/moderatorPanel/PageModeratorPanel';
import PageAccount from '../components/pages/account/PageAccount';
import PageAccountEdit from '../components/pages/accountEdit/PageAccountEdit';
import PageRegistration from '../components/pages/registration/PageRegistration';
import PageLogin from '../components/pages/login/PageLogin';
import PageHome from '../components/pages/home/PageHome';
import PagePosts from '../components/pages/posts/PagePosts';
import PagePostCreate from '../components/pages/postCreate/PagePostCreate';
import PagePostEdit from '../components/pages/postEdit/PagePostEdit';
import PagePostSingle from '../components/pages/postSingle/PagePostSingle';
import PageTodos from '../components/pages/todos/PageTodos';
import PageTodoSingle from '../components/pages/todoSingle/PageTodoSingle';
import PageTodoEdit from '../components/pages/todoEdit/PageTodoEdit';
import PageTodoCreate from '../components/pages/todoCreate/PageTodoCreate';
import PageBlog from '../components/pages/blog/PageBlog';
import PageAbout from '../components/pages/about/PageAbout';
import PageAchievements from '../components/pages/achievements/PageAchievements';
import PageContacts from '../components/pages/contacts/PageContacts';
import PageClients from '../components/pages/clients/PageClients';
import PageHistory from '../components/pages/history/PageHistory';
import PagePartners from '../components/pages/partners/PagePartners';
import PageReviews from '../components/pages/reviews/PageReviews';
import PageTeam from '../components/pages/team/PageTeam';
import PageWhoAreWe from '../components/pages/whoAreWe/PageWhoAreWe';
import PageNotFound from '../components/pages/notFound/PageNotFound';
import RequireAuth from '../components/templates/requireAuth/RequireAuth';
import RequireAdmin from '../components/templates/requireAdmin/RequireAdmin';
import RequireModerator from '../components/templates/requireModerator/RequireModerator';

import { PostProvider } from '../state/posts/postProvider';

const routers = createBrowserRouter([
	{
		path: '/',
		element: <Layout />,
		// errorElement: <PageError />,
		children: [
			{ index: true, element: <PageHome /> },
			{ path: 'registration', element: <PageRegistration /> },
			{ path: 'login', element: <PageLogin /> },
			{ path: 'reg', element: <Navigate to="/registration" replace /> },
			{ path: 'log', element: <Navigate to="/login" replace /> },

			{
				path: 'admin-panel',
				element: (
					<RequireAdmin>
						<PageAdminPanel />
					</RequireAdmin>
				),
			},

			{
				path: 'admin-panel/user/',
				element: (
					<RequireAdmin>
						<PageUser />
					</RequireAdmin>
				),
			},

			{
				path: 'admin-panel/user/create',
				element: (
					<RequireAdmin>
						<PageUserCreate />
					</RequireAdmin>
				),
			},

			{
				path: 'admin-panel/user/edit/:userId',
				element: (
					<RequireAdmin>
						<PageUserEdit />
					</RequireAdmin>
				),
			},

			{
				path: 'admin-panel/user/edit',
				element: <Navigate to="/admin-panel/user" replace />,
			},

			{
				path: 'moderator-panel',
				element: (
					<RequireModerator>
						<PageModeratorPanel />
					</RequireModerator>
				),
			},

			{
				path: 'account',
				element: (
					<RequireAuth>
						<PageAccount />
					</RequireAuth>
				),
			},

			{
				path: 'posts',
				element: (
					<PostProvider>
						<PagePosts />
					</PostProvider>
				),
			},

			{
				path: 'posts/:postId',
				element: (
					<PostProvider>
						<PagePostSingle />
					</PostProvider>
				),
			},

			{
				path: 'posts/new',
				element: (
					<RequireAuth>
						<PostProvider>
							<PagePostCreate />
						</PostProvider>
					</RequireAuth>
				),
			},

			{
				path: 'posts/:postId/edit',
				element: (
					<RequireModerator>
						<PostProvider>
							<PagePostEdit />
						</PostProvider>
					</RequireModerator>
				),
			},

			{
				path: 'account/edit',
				element: (
					<RequireAuth>
						<PageAccountEdit />
					</RequireAuth>
				),
			},

			{
				path: 'todos',
				element: <PageTodos />,
				errorElement: <PageError />,
				loader: todosAllLoader,
			},

			{
				path: 'todos/:todoId',
				element: <PageTodoSingle />,
				loader: todoLoader,
			},

			{
				path: 'todos/:todoId/edit',
				element: <PageTodoEdit />,
				action: actionUpdateTodo,
				loader: todoLoader,
			},

			{
				path: 'todos/create',
				element: <PageTodoCreate />,
				action: actionCreateTodo,
			},

			{
				path: 'blog',
				element: <PageBlog />,
			},

			{
				path: '*',
				element: <PageNotFound />,
			},

			{
				path: 'about',
				element: <PageAbout />,
				children: [
					{
						index: true,
						element: <PageWhoAreWe />,
					},

					{
						path: 'achievements',
						element: <PageAchievements />,
					},

					{
						path: 'contacts',
						element: <PageContacts />,
					},

					{
						path: 'history',
						element: <PageHistory />,
					},

					{
						path: 'partners',
						element: <PagePartners />,
					},

					{
						path: 'reviews',
						element: <PageReviews />,
					},

					{
						path: 'team',
						element: <PageTeam />,
					},

					{
						path: 'clients',
						element: <PageClients />,
					},
				],
			},
		],
	},
]);

export default routers;
