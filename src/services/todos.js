const getAllTodos = async () => {
	const response = await fetch('https://jsonplaceholder.typicode.com/todos/');
	// if (!response.ok) {
	// 	throw new Response('', {
	// 		status: response.status,
	// 		statusText: 'Not found',
	// 		message: 'Error Fetch',
	// 	});
	// }

	const todos = await response.json();

	return todos;
};

const getTodo = async id => {
	const response = await fetch(
		`https://jsonplaceholder.typicode.com/todos/${id}`
	);

	const todo = await response.json();

	return todo;
};

const updateTodo = async todo => {
	const url = `https://jsonplaceholder.typicode.com/todos/${todo.get('id')}`;
	const res = await fetch(url, {
		method: 'PUT',
		body: todo,
	});

	return res.json();
};

const createTodo = async ({ title, userId }) => {
	const res = await fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		body: JSON.stringify({
			title,
			userId,
		}),
		headers: {
			'Content-type': 'application/json; charset=UTF-8',
		},
	});

	const newTodo = await res.json();

	return newTodo;
};

export { getAllTodos, getTodo, updateTodo, createTodo };
