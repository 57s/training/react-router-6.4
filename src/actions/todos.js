import { redirect } from 'react-router-dom';

import { createTodo, updateTodo } from '../services/todos';

export const actionUpdateTodo = async ({ request }) => {
	const formData = await request.formData();
	if (!formData.get('title')) {
		return { message: 'Поле не должно быть пустым' };
	}
	const updateTodoData = await updateTodo(formData);

	return { message: `Todo c id  ${updateTodoData.id} был обновлен` };
};

export const actionCreateTodo = async ({ request }) => {
	const formData = await request.formData();
	const newPost = {
		title: formData.get('title'),
		userId: formData.get('userId'),
	};

	const post = await createTodo(newPost);
	console.log('фейковый сервер, ответ:');
	console.log(post);

	return redirect('todos/' + post.id);
};
