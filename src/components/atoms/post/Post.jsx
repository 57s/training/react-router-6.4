import React from 'react';

import styles from './Post.module.scss';

function Post(props) {
	return (
		<div className={styles.post}>
			<div className={styles.post_num}>{props.index + 1}</div>
			<div className={styles.post_content}>
				<h3 className={styles.post_title}>{props.title}</h3>
				<p className={styles.post_body}>{props.body}</p>
			</div>
		</div>
	);
}
export default Post;
