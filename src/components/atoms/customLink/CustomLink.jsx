import React from 'react';
import { Link, useMatch } from 'react-router-dom';

import styles from './CustomLink.module.scss';
function CustomLink({ children, to, ...props }) {
	const match = useMatch({ path: to, end: to.length === 1 });

	return (
		<Link
			to={to}
			{...props}
			className={match ? `${styles.link} ${styles.link__active}` : styles.link}>
			{children}
		</Link>
	);
}
export default CustomLink;
