import React from 'react';
import { useNavigate } from 'react-router-dom';

import styles from './Todo.module.scss';

function Todo({ id, title, completed }) {
	const [todo, setTodo] = React.useState({ id, title, completed });
	const navigate = useNavigate();
	const changeHandler = event => {
		setTodo({ ...todo, completed: !todo.completed });
	};

	return (
		<div className={styles.todo}>
			<div className={styles.todo_id}>{id}</div>

			<div className={styles.todo_content}>
				<input
					className={styles.todo_input}
					type="checkbox"
					checked={todo.completed}
					onChange={changeHandler}
				/>
				<h3 className={styles.todo_title}>{title}</h3>
			</div>

			<div className={styles.todo_boxButtons}>
				<button onClick={() => navigate(`${id}`)}>Перейти</button>
			</div>
		</div>
	);
}
export default Todo;
