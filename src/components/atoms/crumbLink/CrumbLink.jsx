import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './CrumbLink.module.scss';

const CrumbLink = function ({ url, title }) {
	const getClassLink = ({ isActive }) =>
		isActive
			? `${styles.crumb_link} ${styles.crumb_link__active}`
			: styles.crumb_link;

	return (
		<li className={styles.crumb} key={url}>
			<NavLink className={getClassLink} to={url}>
				{title}
			</NavLink>
		</li>
	);
};

export default CrumbLink;
