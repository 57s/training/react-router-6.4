import React from 'react';

import styles from './Button.module.scss';

function Button({ name, className, ...restProps }) {
	return (
		<button
			className={`${styles.button} ${className ? className : ''}`}
			{...restProps}>
			{name}
		</button>
	);
}
export default Button;
