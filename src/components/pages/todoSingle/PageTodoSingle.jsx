import React, { Suspense } from 'react';
import { Await, Link, useAsyncValue, useLoaderData } from 'react-router-dom';

import styles from './PageTodoSingle.module.scss';

function TodoDetails() {
	const todo = useAsyncValue();

	return (
		<div className={styles.todo}>
			<h3 className={styles.todo_title}>{todo.title}</h3>
			<Link to={`/todos/${todo.id}/edit`}> Редактировать</Link>
		</div>
	);
}

function PageTodoSingle() {
	const todo = useLoaderData();
	return (
		<Suspense fallback={<h2>Загрузка тудушки</h2>}>
			<Await resolve={todo}>
				<TodoDetails />
			</Await>
		</Suspense>
	);
}

export default PageTodoSingle;
