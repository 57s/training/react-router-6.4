import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PageModeratorPanel.module.scss';

function PageModeratorPanel() {
	return (
		<section className={styles.moderatorPanel}>
			<Title title="Модераторка" />
		</section>
	);
}
export default PageModeratorPanel;
