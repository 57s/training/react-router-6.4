import React from 'react';
import { useActionData, useLoaderData, useNavigation } from 'react-router-dom';

import Title from '../../atoms/title/Title';
import FormTodoEdit from '../../molecules/formTodoEdit/FormTodoEdit';
import styles from './PageTodoEdit.module.scss';

function PageTodoEdit() {
	const navigation = useNavigation();
	const todo = useLoaderData();
	const data = useActionData();
	console.log('useActionData', data);

	return (
		<section className={styles.todo}>
			<Title title="Редактировать" />
			{data?.message || <div>{data?.message}</div>}
			<FormTodoEdit {...todo} submitting={navigation.state === 'submitting'} />
		</section>
	);
}
export default PageTodoEdit;
