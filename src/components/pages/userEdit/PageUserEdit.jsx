import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import useCustomNavigate from '../../../hooks/useCustomNavigate';
import { getUser } from '../../../utils/user';
import { AppContext } from '../../../state/app/appProvider';

import Title from '../../atoms/title/Title';
import FormAccount from '../../molecules/formAccount/FormAccount';

import styles from './PageUserEdit.module.scss';

function PageUserEdit() {
	const { userId } = useParams();
	const { users, currentUser, changeUserData, sigIn } =
		React.useContext(AppContext);
	const user = getUser(users, userId);
	const { goBack } = useCustomNavigate();

	const submitHandler = newDataUser => {
		changeUserData(newDataUser);
		goBack();
		if (currentUser.id === newDataUser.id) sigIn(newDataUser.id);
	};

	return (
		<section className={styles.userEdit}>
			<Title title="Изменить аккаунт" />

			<FormAccount
				initData={user}
				isShowTypeChanges="true"
				onSubmit={submitHandler}
				submitName="Изменить"
			/>
		</section>
	);
}
export default PageUserEdit;
