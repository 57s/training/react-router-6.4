import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PageReviews.module.scss';

function PageReviews() {
	return (
		<section className={styles.reviews}>
			<Title title="Отзывы" />
		</section>
	);
}
export default PageReviews;
