import React from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';

import { defaultUser } from '../../../data/defaultData';
import Title from '../../atoms/title/Title';
import FormAccount from '../../molecules/formAccount/FormAccount';
import styles from './PageRegistration.module.scss';

import { AppContext } from '../../../state/app/appProvider';

function PageRegistration() {
	const navigate = useNavigate();
	const location = useLocation();
	const fromPage = location.state?.from || '/';
	const { addUser } = React.useContext(AppContext);

	const submitHandler = formData => {
		addUser(formData);
		navigate('/login', { state: { from: fromPage } });
	};

	return (
		<section className={styles.registration}>
			<Title title="Registration" />
			<Link to="/login">Вход</Link>

			<FormAccount
				initData={defaultUser}
				onSubmit={submitHandler}
				submitName="Зарегистрироваться"
			/>
		</section>
	);
}
export default PageRegistration;
