import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PageBlog.module.scss';

function PageBlog() {
	return (
		<section className={styles.blog}>
			<Title title="Blog" />
		</section>
	);
}
export default PageBlog;
