import React from 'react';
import { useNavigate } from 'react-router-dom';

import { defaultPost } from '../../../data/defaultData';
import { PostContext } from '../../../state/posts/postProvider';
import Title from '../../atoms/title/Title';
import FormPost from '../../molecules/formPost/FormPost';
import styles from './PagePostCreate.module.scss';

function PagePostCreate() {
	const navigate = useNavigate();
	const { createPost } = React.useContext(PostContext);

	const handlerDataPost = dataPost => {
		const newPost = { id: window.crypto.randomUUID(), userId: 0, ...dataPost };
		createPost(newPost);

		setTimeout(() => {
			navigate('/posts/');
		}, 0);
	};

	return (
		<section className={styles.post}>
			<Title title="Создать пост" />

			<FormPost
				postInitData={defaultPost}
				handler={handlerDataPost}
				buttonName="Добавить"
			/>
		</section>
	);
}
export default PagePostCreate;
