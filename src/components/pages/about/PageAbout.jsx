import React from 'react';
import { Outlet, Link } from 'react-router-dom';
import Title from '../../atoms/title/Title';
import styles from './PageAbout.module.scss';

function PageAbout() {
	return (
		<section className={styles.about}>
			<Title title="About" />
			<div className={styles.about_wrapper}>
				<Outlet />

				<aside className={styles.about_aside}>
					<Link to="">О нас</Link>
					<Link to="team">Команда</Link>
					<Link to="partners">Партнеры</Link>
					<Link to="clients">Клиенты</Link>
					<Link to="reviews">Отзывы</Link>
					<Link to="contacts">Контакты</Link>
					<Link to="history">История</Link>
					<Link to="achievements">Достижения</Link>
				</aside>
			</div>
		</section>
	);
}
export default PageAbout;
