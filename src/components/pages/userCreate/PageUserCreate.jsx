import React from 'react';

import { defaultUser } from '../../../data/defaultData';
import { AppContext } from '../../../state/app/appProvider';
import Title from '../../atoms/title/Title';
import FormAccount from '../../molecules/formAccount/FormAccount';
import styles from './PageUserCreate.module.scss';

function PageUserCreate() {
	const { addUser } = React.useContext(AppContext);

	const submitHandler = (formData, onClear) => {
		addUser(formData);
		onClear(defaultUser);
	};

	return (
		<section className={styles.userEdit}>
			<Title title="Создать пользователя" />

			<FormAccount
				initData={defaultUser}
				isShowTypeChanges="true"
				onSubmit={submitHandler}
				submitName="Создать"
			/>
		</section>
	);
}
export default PageUserCreate;
