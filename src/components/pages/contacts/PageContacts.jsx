import React from 'react';

import styles from './PageContacts.module.scss';
import Title from '../../atoms/title/Title';

function PageContacts() {
	return (
		<section className={styles.contacts}>
			<Title title="Контакты" />
		</section>
	);
}
export default PageContacts;
