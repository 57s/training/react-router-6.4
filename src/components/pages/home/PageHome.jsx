import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PageHome.module.scss';

function PageHome() {
	return (
		<section className={styles.home}>
			<Title title="Home" />
		</section>
	);
}
export default PageHome;
