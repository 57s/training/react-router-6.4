import React from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';

import { PostContext } from '../../../state/posts/postProvider';

import Title from '../../atoms/title/Title';
import Button from '../../atoms/button/Button';
import styles from './PagePostSingle.module.scss';
import useAuth from '../../../hooks/useAuth';

function PagePostSingle() {
	const { isAdmin } = useAuth();
	const navigate = useNavigate();
	const { postId } = useParams();
	const { posts, deletePost } = React.useContext(PostContext);
	const post = posts.find(post => String(post.id) === postId);

	const deleteHandler = () => {
		deletePost(postId);
		navigate('/posts/');
	};

	return (
		<section className={styles.post}>
			<Title title={`Post № ${postId}`} />
			{post && (
				<div className={styles.post_box}>
					<div className={styles.post_title}>{post.title}</div>
					<div className={styles.post_body}>{post.body}</div>
					<div className={styles.post_buttonBox}>
						<div className={styles.post_buttonEdit}>
							<Link to="edit">Редактировать</Link>
						</div>
						<div className={styles.post_buttonDelete}>
							<Button
								onClick={deleteHandler}
								name="Удалить"
								disabled={!isAdmin}
								title={
									isAdmin
										? 'Удалить пост'
										: 'Необходимо авторизоваться под администратором'
								}
							/>
						</div>
					</div>
				</div>
			)}
		</section>
	);
}
export default PagePostSingle;
