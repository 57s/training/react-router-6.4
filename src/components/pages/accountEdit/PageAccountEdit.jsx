import React from 'react';
import { useNavigate } from 'react-router-dom';

import { AppContext } from '../../../state/app/appProvider';
import Title from '../../atoms/title/Title';
import FormAccount from '../../molecules/formAccount/FormAccount';
import styles from './PageAccountEdit.module.scss';

function PageAccountEdit() {
	const navigate = useNavigate();
	const { currentUser, changeUserData, sigIn } = React.useContext(AppContext);

	const submitHandler = data => {
		changeUserData(data);
		sigIn(data.id);
		navigate(-1);
	};

	return (
		<section className={styles.account}>
			<Title title="Изменить аккаунт" />
			<FormAccount
				initData={currentUser}
				onSubmit={submitHandler}
				submitName="Сохранить"
			/>
		</section>
	);
}
export default PageAccountEdit;
