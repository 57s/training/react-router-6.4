import React, { Suspense } from 'react';
import { Await, useLoaderData, Link } from 'react-router-dom';

import Todo from '../../atoms/todo/Todo';
import styles from './PageTodos.module.scss';
import Title from '../../atoms/title/Title';

function PageTodos() {
	const { todos } = useLoaderData();
	// if (!todos) return;

	return (
		<section className={styles.todos}>
			<Title title="Дела" />

			<Link to="create">Создать</Link>
			<div className={styles.todos_content}>
				<Suspense fallback={<h2>Загрузка ...</h2>}>
					<Await resolve={todos}>
						{resolvedTodos =>
							resolvedTodos.map(todo => <Todo key={todo.id} {...todo} />)
						}
					</Await>
				</Suspense>
			</div>
		</section>
	);
}

export default PageTodos;
