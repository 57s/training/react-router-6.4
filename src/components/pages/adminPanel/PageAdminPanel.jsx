import React from 'react';
import { Link } from 'react-router-dom';

import Title from '../../atoms/title/Title';
import styles from './PageAdminPanel.module.scss';

function PageAdminPanel() {
	return (
		<section className={styles.adminPanel}>
			<Title title="Админка" />

			<div className={styles.adminPanel_inner}>
				<Link to="user">Users</Link>
			</div>
		</section>
	);
}
export default PageAdminPanel;
