import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PageAchievements.module.scss';

function PageAchievements() {
	return (
		<section className={styles.achievements}>
			<Title title="Достижения" />
		</section>
	);
}
export default PageAchievements;
