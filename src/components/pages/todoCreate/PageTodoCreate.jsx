import React from 'react';
import { useNavigation } from 'react-router-dom';

import styles from './PageTodoCreate.module.scss';
import Title from '../../atoms/title/Title';
import FormTodoNew from '../../molecules/formTodoNew/FormTodoNew';

function PageTodoCreate() {
	const navigation = useNavigation();
	return (
		<section className={styles.todo}>
			<Title title="Создать" />

			<div className={styles.todo_content}>
				<FormTodoNew submitting={navigation.state === 'submitting'} />
			</div>
		</section>
	);
}
export default PageTodoCreate;
