import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PageTeam.module.scss';

function PageTeam() {
	return (
		<section className={styles.team}>
			<Title title="Команда" />
		</section>
	);
}
export default PageTeam;
