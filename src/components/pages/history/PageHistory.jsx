import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PageHistory.module.scss';

function PageHistory() {
	return (
		<section className={styles.history}>
			<Title title="История" />
		</section>
	);
}
export default PageHistory;
