import React from 'react';

import styles from './PageWhoAreWe.module.scss';
import Title from '../../atoms/title/Title';

function PageWhoAreWe() {
	return (
		<section className={styles.whoAreWe}>
			<Title title="О нас" />
		</section>
	);
}
export default PageWhoAreWe;
