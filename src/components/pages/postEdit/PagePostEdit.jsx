import React from 'react';
import { useParams, useNavigate } from 'react-router-dom';

import { PostContext } from '../../../state/posts/postProvider';
import Title from '../../atoms/title/Title';
import FormPost from '../../molecules/formPost/FormPost';
import styles from './PagePostEdit.module.scss';

function PagePostEdit() {
	const navigate = useNavigate();
	const { postId } = useParams();
	const { posts, updatePost } = React.useContext(PostContext);
	const initPost = posts.find(post => String(post.id) === postId);

	const submitHandler = data => {
		updatePost(data);

		setTimeout(() => {
			navigate('/posts/');
		}, 0);
	};

	return (
		<section className={styles.post}>
			<Title title={`Редактировать пост № ${postId}`} />

			<FormPost
				postInitData={initPost}
				buttonName="Сохранить"
				handler={submitHandler}
			/>
		</section>
	);
}
export default PagePostEdit;
