import React from 'react';
import { Link } from 'react-router-dom';

import { getUser } from '../../../utils/user';
import { AppContext } from '../../../state/app/appProvider';

import Button from '../../atoms/button/Button';
import Title from '../../atoms/title/Title';
import styles from './PageUser.module.scss';

function PageUser() {
	const { users, currentUser, removeUser, sigIn } =
		React.useContext(AppContext);
	const [selectedUser, setSelectedUser] = React.useState(currentUser);
	const [isShowPassword, setShowPassword] = React.useState(false);

	const toggleShowPassword = () => setShowPassword(!isShowPassword);

	const changeUserHandler = event => {
		setSelectedUser(getUser(users, event.target.value));
	};

	const removeUserHandler = () => {
		removeUser(selectedUser.id);
		if (currentUser.id === selectedUser.id) sigIn('');
	};

	return (
		<section className={styles.user}>
			<Title title={selectedUser.name} />

			<div>
				<div className={styles.user_selectBox}>
					<select value={selectedUser.id} onChange={changeUserHandler}>
						{users.map(user => (
							<option key={user.id} value={user.id}>
								{user.name}
							</option>
						))}
					</select>
				</div>

				<div className={styles.user_content}>
					<div className={styles.user_info}>
						<p>Name: {selectedUser.name}</p>
						<p>Login: {selectedUser.login}</p>
						<div className={styles.user_passwordBox}>
							<p>
								Password:
								<span
									className={
										isShowPassword
											? styles.user_passwordText
											: `${styles.user_passwordText} ${styles.user_passwordText__hidden}`
									}>
									{selectedUser.password}
								</span>
							</p>
							<button
								className={styles.user_passwordShowButton}
								onClick={toggleShowPassword}>
								{isShowPassword ? 'Скрыть' : 'Показать'}
							</button>
						</div>
						<p>Type: {selectedUser.type}</p>
						<p>Date: {new Date(selectedUser.timeCreation).toLocaleString()}</p>
					</div>

					<aside className={styles.user_navigation}>
						<Link to={`edit/${selectedUser.id}`}>Изменить</Link>
						<Link to="Create">Создать</Link>

						<Button onClick={removeUserHandler} name="Удалить"></Button>
					</aside>
				</div>
			</div>
		</section>
	);
}

export default PageUser;
