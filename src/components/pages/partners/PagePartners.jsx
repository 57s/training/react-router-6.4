import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PagePartners.module.scss';

function PagePartners() {
	return (
		<section className={styles.partners}>
			<Title title="Партнеры" />
		</section>
	);
}
export default PagePartners;
