import React from 'react';

import Title from '../../atoms/title/Title';
import styles from './PageNotFound.module.scss';
import imgNotFound from './img/notFound.gif';

function PageNotFound() {
	return (
		<section className={styles.notFound}>
			<Title title="Не чего не найдено" />

			<img className={styles.notFound_img} src={imgNotFound} alt="Not Found" />
		</section>
	);
}
export default PageNotFound;
