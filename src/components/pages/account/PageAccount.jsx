import React from 'react';
import { useNavigate, Link } from 'react-router-dom';

import { AppContext } from '../../../state/app/appProvider';
import useAuth from '../../../hooks/useAuth';

import Title from '../../atoms/title/Title';
import Button from '../../atoms/button/Button';
import styles from './PageAccount.module.scss';

function PageAccount() {
	const navigate = useNavigate();
	const { userName, accountType } = useAuth();
	const { sigOut } = React.useContext(AppContext);

	const sigOutHandler = () => {
		sigOut();
		navigate('/');
	};

	return (
		<section className={styles.account}>
			<Title title={`Личный кабинет ${userName}`} />

			<h3 className={styles.account_type}>{`Тип аккаунта: ${accountType}`}</h3>
			<div className={styles.account_content}>
				<Link to="edit">Редактировать</Link>
				<Button onClick={sigOutHandler} name="Выйти" />
			</div>
		</section>
	);
}
export default PageAccount;
