import React from 'react';
import { Link, useSearchParams } from 'react-router-dom';

import { PostContext } from '../../../state/posts/postProvider';
import useAuth from '../../../hooks/useAuth';

import Title from '../../atoms/title/Title';
import Post from '../../atoms/post/Post';
import FormPostControl from '../../molecules/formPostControl/FormPostControl';
import styles from './PagePosts.module.scss';

function PagePosts() {
	const { isUser } = useAuth();
	const { posts } = React.useContext(PostContext);
	const [urlParams, setSearchParams] = useSearchParams();
	const startParams = { search: '', userId: 'false' };

	const handleFormControl = params => {
		const newParams = {};

		if (params.search.length > 0) newParams.search = params.search;
		if (params.userId !== 'false') newParams.userId = params.userId;

		setSearchParams(newParams);
	};

	for (const [key, value] of urlParams.entries()) {
		startParams[key] = value;
	}

	let postArray = posts.filter(post => post.title.includes(startParams.search));

	if (startParams.userId !== 'false') {
		postArray = postArray.filter(
			post => String(post.userId) === startParams.userId
		);
	}

	return (
		<section className={styles.posts}>
			<Title title="Posts" />

			<div className={styles.posts_controlBox}>
				<FormPostControl
					initDataForm={startParams}
					onSubmit={handleFormControl}
				/>

				{isUser && (
					<Link to={'new'}>
						<div className={styles.posts_add}>
							<div className={styles.posts_addText}>Добавить пост</div>
						</div>
					</Link>
				)}
			</div>

			{postArray.map((post, index) => (
				<Link key={post.id} to={`${post.id}`}>
					<Post {...post} index={index} />
				</Link>
			))}
		</section>
	);
}
export default PagePosts;
