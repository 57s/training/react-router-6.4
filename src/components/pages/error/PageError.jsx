import React from 'react';
import { isRouteErrorResponse, useRouteError } from 'react-router-dom';

import Title from '../../atoms/title/Title';
import styles from './PageError.module.scss';

function PageError() {
	const error = useRouteError();

	if (isRouteErrorResponse(error)) {
		return (
			<section className={styles.error}>
				<Title title="Error" />

				<div>
					<h1>{error.status}</h1>
					<h1>{error.statusText || 'Something goes wrong'}</h1>
					<h3>{error.data.message || 'Fallback Error'}</h3>
					<p>{error.data.reason}</p>
				</div>
			</section>
		);
	}

	return <div>Ошибка не в роуторе</div>;
}
export default PageError;
