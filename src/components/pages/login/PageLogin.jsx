import React from 'react';
import { useLocation, useNavigate, Link } from 'react-router-dom';

import { AppContext } from '../../../state/app/appProvider';
import Title from '../../atoms/title/Title';
import styles from './PageLogin.module.scss';

function PageLogin() {
	const { users, sigIn } = React.useContext(AppContext);
	const navigate = useNavigate();
	const location = useLocation();
	const fromPage = location.state?.from || '/';

	const initFormData = { login: '', password: '' };
	const [formData, setFormData] = React.useState(initFormData);

	const changeLogin = event => {
		setFormData({ ...formData, login: event.target.value });
	};

	const changePassword = event => {
		setFormData({ ...formData, password: event.target.value });
	};

	const submitHandler = event => {
		event.preventDefault();
		const currentUser = users.find(user => user.login === formData.login);

		if (!currentUser) return alert('Нет такого пользователя');

		if (currentUser.password === formData.password) {
			sigIn(currentUser.id);
			navigate(fromPage);
		} else {
			alert('Не правильный пароль');
		}
	};

	return (
		<section className={styles.login}>
			<Title title="Login" />
			<Link to="/registration">Регистрация</Link>

			<form className={styles.login_form} onSubmit={submitHandler}>
				<label className={styles.login_label}>
					<h4 className={styles.login_title}>Login:</h4>
					<input
						className={styles.login_input}
						type="email"
						required
						value={formData.login}
						onChange={changeLogin}
					/>
				</label>

				<label className={styles.login_label}>
					<h4 className={styles.login_title}>Password:</h4>
					<input
						className={styles.login_input}
						type="password"
						required
						value={formData.password}
						onChange={changePassword}
					/>
				</label>

				<input className={styles.login_submit} type="submit" value="Войти" />
			</form>
		</section>
	);
}
export default PageLogin;
