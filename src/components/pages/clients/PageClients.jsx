import React from 'react';

import styles from './PageClients.module.scss';
import Title from '../../atoms/title/Title';

function PageClients() {
	return (
		<section className={styles.clients}>
			<Title title="Клиенты" />
		</section>
	);
}
export default PageClients;
