import React from 'react';
import { useLocation, Link } from 'react-router-dom';

import useAuth from '../../../hooks/useAuth';
import styles from './RequireAuth.module.scss';

function RequireAuth({ children }) {
	const location = useLocation();
	const { isUser } = useAuth();
	if (isUser) return children;

	return (
		<div className={styles.requireAuth}>
			<h3 className={styles.requireAuth_title}>
				Для доступа к этому разделу нужна авторизация
			</h3>

			<div className={styles.requireAuth_boxButton}>
				<Link to="/login" state={{ from: location.pathname }}>
					Login
				</Link>

				<Link to="/registration" state={{ from: location.pathname }}>
					Registration
				</Link>
			</div>
		</div>
	);
}
export default RequireAuth;
