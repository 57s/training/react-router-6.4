import React from 'react';

import useCustomNavigate from '../../../hooks/useCustomNavigate';
import useAuth from '../../../hooks/useAuth';
import Button from '../../atoms/button/Button';
import styles from './RequireAdmin.module.scss';

function RequireAdmin({ children }) {
	const { isAdmin } = useAuth();
	const { goBack, goHome } = useCustomNavigate();

	if (isAdmin) return children;

	return (
		<div className={styles.requireAdmin}>
			<h3 className={styles.requireAdmin_title}>
				Для доступа к этому разделу нужны привилегии администратора
			</h3>

			<div className={styles.requireAdmin_boxButton}>
				<Button onClick={goHome} name="Домой" />
				<Button onClick={goBack} name="Назад" />
			</div>
		</div>
	);
}
export default RequireAdmin;
