import React from 'react';

import useCustomNavigate from '../../../hooks/useCustomNavigate';
import useAuth from '../../../hooks/useAuth';
import Button from '../../atoms/button/Button';
import styles from './RequireModerator.module.scss';

function RequireModerator({ children }) {
	const { isModerator, isAdmin } = useAuth();
	const { goBack, goHome } = useCustomNavigate();

	if (isModerator || isAdmin) return children;

	return (
		<div className={styles.requireModerator}>
			<h3 className={styles.requireModerator_title}>
				Для доступа к этому разделу нужны привилегии модератора
			</h3>

			<div className={styles.requireModerator_boxButton}>
				<Button onClick={goHome} name="Домой" />
				<Button onClick={goBack} name="Назад" />
			</div>
		</div>
	);
}
export default RequireModerator;
