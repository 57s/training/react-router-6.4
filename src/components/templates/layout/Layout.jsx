import React from 'react';

import DevToolsButton from '../../../dev/components/devToolsButton/DevToolsButton';
import DevToolsPanel from '../../../dev/components/devToolsPanel/DevToolsPanel';
import Breadcrumb from '../../molecules/breadcrumb/Breadcrumb';
import Header from '../../organisms/header/Header';
import Main from '../main/Main';
import Footer from '../../organisms/footer/Footer';
import styles from './Layout.module.scss';
import useContentVisibilityUrl from '../../../hooks/useContentVisibilityUrl';

function Layout() {
	const { isShow: isShowBreadcrumb } = useContentVisibilityUrl(['/']);
	const [isShowAdminPanel, setShowAdminPanel] = React.useState(false);

	const handlerToggleAdminPanelButton = () =>
		setShowAdminPanel(!isShowAdminPanel);

	return (
		<div className={styles.page}>
			<Header className={styles.page_header} />
			{isShowBreadcrumb && <Breadcrumb />}
			<Main className={styles.page_main} />
			<Footer className={styles.page_footer} />

			<DevToolsButton onClick={handlerToggleAdminPanelButton} />
			<DevToolsPanel
				isShow={isShowAdminPanel}
				onToggle={handlerToggleAdminPanelButton}
			/>
		</div>
	);
}
export default Layout;
