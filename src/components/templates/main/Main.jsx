import React from 'react';
import { Outlet } from 'react-router-dom';

import Container from '../container/Container';
import styles from './Main.module.scss';
function Main({ className }) {
	return (
		<main className={`${styles.main} ${className ? className : ''} `}>
			<Container>
				<Outlet />
			</Container>
		</main>
	);
}
export default Main;
