import React from 'react';

import Container from '../../templates/container/Container';
import styles from './Footer.module.scss';

function Footer({ className }) {
	return (
		<footer className={`${styles.footer} ${className ? className : ''}`}>
			<Container>Footer</Container>
		</footer>
	);
}
export default Footer;
