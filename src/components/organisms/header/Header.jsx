import React from 'react';
import { useNavigate } from 'react-router-dom';

import useContentVisibilityUrl from '../../../hooks/useContentVisibilityUrl';
import useAuth from '../../../hooks/useAuth';

import CustomLink from '../../atoms/customLink/CustomLink';
import Container from '../../templates/container/Container';
import Button from '../../atoms/button/Button';
import styles from './Header.module.scss';

function Header({ className }) {
	const { isShow: isShowButton } = useContentVisibilityUrl([
		'/login',
		'/registration',
		'/account',
		'/account/edit',
	]);

	const navigate = useNavigate();
	const { isUser, isModerator, isAdmin, userName } = useAuth();
	const toLoginPage = () => navigate('/login');
	const toAccountPage = () => navigate('/account');

	return (
		<header className={`${styles.header} ${className ? className : ''}`}>
			<Container>
				<div className={styles.header_inner}>
					<div className={styles.header_logo}></div>

					<nav className={styles.header_menu}>
						<CustomLink to="/">Home</CustomLink>
						<CustomLink to="/todos">Todos</CustomLink>
						<CustomLink to="/posts">Posts</CustomLink>
						<CustomLink to="/blog">Blog</CustomLink>
						<CustomLink to="/about">About</CustomLink>

						{isModerator && (
							<>
								<CustomLink to="moderator-panel">Модераторка</CustomLink>
							</>
						)}

						{isAdmin && (
							<>
								<CustomLink to="admin-panel">Админка</CustomLink>
							</>
						)}
					</nav>

					<div className={styles.header_buttonBox}>
						{isShowButton && (
							<Button
								className={styles.header_button}
								onClick={isUser ? toAccountPage : toLoginPage}
								name={isUser ? userName : 'Гость'}
								title={isUser ? 'Управление аккаунтом' : 'Войти'}
							/>
						)}
					</div>
				</div>
			</Container>
		</header>
	);
}
export default Header;
