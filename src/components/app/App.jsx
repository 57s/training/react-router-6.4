import React from 'react';
import { RouterProvider } from 'react-router-dom';

import { AppProvider } from '../../state/app/appProvider';
import routers from '../../routes/routes';

function App() {
	return (
		<AppProvider>
			<RouterProvider router={routers} />
		</AppProvider>
	);
}
export default App;
