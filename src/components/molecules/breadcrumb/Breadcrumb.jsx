import React from 'react';
import { useLocation } from 'react-router-dom';

import useCustomNavigate from '../../../hooks/useCustomNavigate';

import { ReactComponent as BackSvg } from '../../../img/backbutton.svg';
import { ReactComponent as HomeSvg } from '../../../img/home.svg';
import Container from '../../templates/container/Container';
import CrumbLink from '../../atoms/crumbLink/CrumbLink';

import styles from './Breadcrumb.module.scss';

function Breadcrumb() {
	const { goBack } = useCustomNavigate();
	const location = useLocation();
	const homeBreadCrumb = { url: '/', title: <HomeSvg /> };

	let currentUrl = '';
	const arrayBreadCrumbs = location.pathname
		.split('/')
		.filter(crumb => crumb !== '')
		.map(crumb => {
			currentUrl += `/${crumb}`;
			return { url: currentUrl, title: crumb };
		});

	return (
		<div className={styles.breadCrumb}>
			<Container>
				<nav className={styles.breadCrumb_inner}>
					<button
						className={styles.breadCrumb_back}
						onClick={goBack}
						title="Назад">
						<BackSvg />
					</button>

					<ul className={styles.breadCrumb_container}>
						<CrumbLink {...homeBreadCrumb} />
						{arrayBreadCrumbs.map(crumb => (
							<CrumbLink {...crumb} key={crumb.url} />
						))}
					</ul>
				</nav>
			</Container>
		</div>
	);
}
export default Breadcrumb;
