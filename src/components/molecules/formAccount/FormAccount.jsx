import React from 'react';

import { changeObjectProperty } from '../../../utils/utils';
import styles from './FormAccount.module.scss';

function FormAccount({
	submitName,
	onSubmit,
	initData,
	isShowTypeChanges = false,
}) {
	const [formData, setFormData] = React.useState(initData);

	const changePropertyForm = (property, event) => {
		setFormData(changeObjectProperty(formData, property, event.target.value));
	};

	const submitHandler = event => {
		event.preventDefault();
		onSubmit(formData, setFormData);
	};

	return (
		<form className={styles.registrationForm} onSubmit={submitHandler}>
			<label className={styles.registration_label}>
				<h4 className={styles.registration_title}>Name:</h4>
				<input
					className={styles.registration_input}
					type="text"
					required
					value={formData.name}
					onChange={changePropertyForm.bind({}, 'name')}
				/>
			</label>

			<label className={styles.registration_label}>
				<h4 className={styles.registration_title}>Login:</h4>
				<input
					className={styles.registration_input}
					type="email"
					required
					value={formData.login}
					onChange={changePropertyForm.bind({}, 'login')}
				/>
			</label>

			<label className={styles.registration_label}>
				<h4 className={styles.registration_title}>Password:</h4>
				<input
					className={styles.registration_input}
					type="password"
					required
					value={formData.password}
					onChange={changePropertyForm.bind({}, 'password')}
				/>
			</label>

			{isShowTypeChanges && (
				<label className={styles.registration_label}>
					<h4 className={styles.registration_title}>Account type:</h4>
					<select
						value={formData.type}
						onChange={changePropertyForm.bind({}, 'type')}
						required>
						<option value="user">User</option>
						<option value="moderator">Moderator</option>
						<option value="admin">Admin</option>
					</select>
				</label>
			)}

			<input
				className={styles.registration_submit}
				type="submit"
				value={submitName}
			/>
		</form>
	);
}
export default FormAccount;
