import React from 'react';

import styles from './FormPostControl.module.scss';

function FormPostControl({ initDataForm, onSubmit }) {
	const [formData, setFormData] = React.useState(initDataForm);

	const onChangeSearch = event =>
		setFormData({ ...formData, search: event.target.value });

	const onChangeUserId = event =>
		setFormData({ ...formData, userId: event.target.value });

	const handleSubmit = event => {
		event.preventDefault();
		onSubmit(formData);
	};

	return (
		<form className={styles.formPostControl}>
			<label>
				<input
					type="search"
					value={formData.search}
					onChange={onChangeSearch}
				/>
			</label>

			<label>
				<select value={formData.userId} onChange={onChangeUserId}>
					<option value="false">All Users</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">9</option>
					<option value="10">10</option>
				</select>
			</label>

			<input type="submit" value="Фильтровать" onClick={handleSubmit} />
		</form>
	);
}
export default FormPostControl;
