import React from 'react';
import { Form } from 'react-router-dom';

import styles from './FormTodoNew.module.scss';

function FormTodoNew({ submitting }) {
	return (
		<Form action="/todos/create" method="post">
			<label>
				<h3>Title</h3>
				<input type="text" name="title" />
			</label>

			<input type="hidden" name="userId" value="0" />
			<input type="submit" value="Add Todo" disabled={submitting} />
		</Form>
	);
}
export default FormTodoNew;
