import React from 'react';
import { Form } from 'react-router-dom';

import styles from './FormTodoEdit.module.scss';

function FormTodoEdit({ id, title, completed, userId, submitting }) {
	return (
		<Form action={`/todos/${id}/edit`} method="put">
			<label>
				<h3>Title</h3>
				<input type="text" name="title" defaultValue={title} />
			</label>

			<input type="hidden" name="id" value={id} />
			<input type="hidden" name="completed" value={completed} />
			<input type="hidden" name="userId" value={userId} />
			<input type="submit" value="Update Todo" disabled={submitting} />
		</Form>
	);
}
export default FormTodoEdit;
