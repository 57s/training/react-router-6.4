import React from 'react';

import styles from './FormPost.module.scss';

function FormPost({
	postInitData = { title: '', body: '' },
	buttonName = 'Применить',
	handler,
}) {
	const [postData, setPostData] = React.useState(postInitData);

	React.useEffect(() => {
		setPostData(postInitData);
	}, [postInitData]);

	const changeTitle = event =>
		setPostData({ ...postData, title: event.target.value });

	const changeBody = event =>
		setPostData({ ...postData, body: event.target.value });

	const submitHandler = event => {
		event.preventDefault();
		handler(postData);
	};

	return (
		<form className={styles.formPost} onSubmit={submitHandler}>
			<label className={styles.formPost_label}>
				<h3 className={styles.formPost_title}> Title</h3>
				<input
					className={styles.formPost_input}
					type="text"
					value={postData.title}
					onChange={changeTitle}
				/>
			</label>

			<label className={styles.formPost_label}>
				<h3 className={styles.formPost_title}>Description</h3>
				<textarea
					className={styles.formPost_textarea}
					type="text"
					value={postData.body}
					onChange={changeBody}
				/>
			</label>

			<input
				className={styles.formPost_submit}
				type="submit"
				value={buttonName}
			/>
		</form>
	);
}
export default FormPost;
